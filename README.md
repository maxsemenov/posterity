# Posterity code challenge

## Code structure

There are two main files:

- app.js — contains a node script, that reads file path from the CLI, reads file, and evaluates a table
- table.js — contains functions to parse and evaluate table

Besides those, there are test files to unit test table/postix utilities (I also used those for TDD, to gat a faster feedback).

## Callouts

- The postix evaluation assumes, that each operation consist only of two argument and operation, i.e `1 2 +`, not the `1 2 3 +`.
- For cyclic references, the code will throw a specific `CyclicReference` error, that will result in table cell having a value with pointer to a cyclic dependency, i.e `#CYCLIC: d2 -> a2 -> d1`, so it can be fixed.
- Script relies on some latest features of node (like optional chaining and modules), so the node version of `14` and up is required.

## Run script

- `node app.js ./test.csv`
- `node app.js ./testCyclic.csv`
