import { parsePostix } from './table'

describe('parsePostix', () => {
	test('correctly parses simple postix', () => {
		expect(parsePostix('3 5 +')).toEqual(['3', '5', '+'])
	})

	test('correctly parses simple postix with more spaces', () => {
		expect(parsePostix(' 3   5    +')).toEqual(['3', '5', '+'])
	})

	test('correctly parses postix with just one operand', () => {
		expect(parsePostix(' 3')).toEqual(['3'])
	})

	test('correctly parses one-level deep postix', () => {
		expect(parsePostix('4 5 6 + -')).toEqual(['4', ['5', '6', '+'], '-'])
	})

	test('correctly parses complex postix', () => {
		expect(parsePostix('5   1 2 + 4 * + 3 - ')).toEqual([
			['5', [['1', '2', '+'], '4', '*'], '+'],
			'3',
			'-'
		])
	})

	test('correctly parses complex postix with cell reference', () => {
		expect(parsePostix('2 b2 3 * -')).toEqual(['2', ['b2', '3', '*'], '-'])
	})

	test('throws error for incorrect postix', () => {
		expect(() => parsePostix().toThrow())
		expect(() => parsePostix(null).toThrow())
		expect(() => parsePostix('').toThrow())
		expect(() => parsePostix('3 4').toThrow())
		expect(() => parsePostix('3 *').toThrow())
		expect(() => parsePostix('* 4 0').toThrow())
		expect(() => parsePostix('3 - 4').toThrow())
		expect(() => parsePostix(' * - 4').toThrow())
		expect(() => parsePostix('3 4 - 4').toThrow())
		expect(() => parsePostix('3 4 - +').toThrow())
	})
})
