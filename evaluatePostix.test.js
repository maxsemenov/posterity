import { evaluatePostix } from './table'

describe('evaluatePostix', () => {
	test('correctly evaluates postix with just one operand', () => {
		expect(evaluatePostix(['5'])).toBe(5)
	})

	test('correctly evaluates simple postix with addition', () => {
		expect(evaluatePostix(['5', '6', '+'])).toBe(11)
	})

	test('correctly evaluates simple postix with subtraction', () => {
		expect(evaluatePostix(['5', '6', '-'])).toBe(-1)
	})

	test('correctly evaluates simple postix with multiplication', () => {
		expect(evaluatePostix(['5', '6', '*'])).toBe(30)
	})

	test('correctly evaluates simple postix with division', () => {
		expect(evaluatePostix(['6', '3', '/'])).toBe(2)
	})

	test('correctly evaluates one-level deep postix', () => {
		expect(evaluatePostix(['4', ['5', '6', '+'], '-'])).toBe(-7)
	})

	test('correctly evaluates complex postix', () => {
		expect(
			evaluatePostix([['5', [['1', '2', '+'], '4', '*'], '+'], '3', '-'])
		).toBe(14)
	})

	test('corectly evaluates postix with custom value getter', () => {
		expect(
			evaluatePostix(['should be 5', 'should be 6', '+'], (value) =>
				Number(value.replace('should be ', ''))
			)
		).toBe(11)
	})

	test('correctly evaluates complex postix with custom getter', () => {
		expect(
			evaluatePostix(
				[
					['should be 5', [['1', '2', '+'], 'should be 4', '*'], '+'],
					'should be 3',
					'-'
				],
				(value) => Number(value.replace('should be ', ''))
			)
		).toBe(14)
	})
})
