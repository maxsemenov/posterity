import { resolve } from 'path'
import { readFile } from 'fs/promises'

import { evaluateTable } from './table.js'

const parseTable = (str) => str.split('\n').map((row) => row.split(','))

const printTable = (table) => {
	for (const row of table) {
		console.log(row)
	}
}

const app = async () => {
	const [, , filePath] = process.argv

	if (!filePath) {
		console.error('Please pass a path to .csv file as an CLI argument')
	}

	const fullPath = resolve(process.cwd(), filePath)
	let file
	try {
		file = String(await readFile(fullPath))
	} catch (err) {
		console.error(err)
	}
	const table = parseTable(file)
	const evaluatedTable = evaluateTable(table)
	printTable(evaluatedTable)
}

app()
