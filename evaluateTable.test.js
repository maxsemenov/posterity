import { evaluateTable } from './table'

describe('evaluateTable', () => {
	test('correctly evaluates table with just one row', () => {
		const table = [['5', '6', ' ', '5 6 -']]
		expect(evaluateTable(table)).toEqual([[5, 6, 0, -1]])
	})

	test('correctly evaluates table with multiple rows row', () => {
		const table = [
			['5', '6', , '5 6 -'],
			['2 3 *', '12 ', , '5 1 2 + 4 * + 3 -', '345']
		]

		expect(evaluateTable(table)).toEqual([
			[5, 6, 0, -1],
			[6, 12, 0, 14, 345]
		])
	})

	test('correctly evaluates table with error cells', () => {
		const table = [
			['5', '6 5 &', , '5 6 -'],
			['3 *', '12 ', '5 1 2 + 4 * + 3 -', 'true']
		]
		expect(evaluateTable(table)).toEqual([
			[5, '#ERR', 0, -1],
			['#ERR', 12, 14, '#ERR']
		])
	})

	test('correctly evaluates table with referenced cells', () => {
		const table = [
			['5', '6 b2 +', , '5 6 -'],
			['3 c6 *', 'e234', '5 1 2 + 4 * + 3 -', 'a1']
		]
		expect(evaluateTable(table)).toEqual([
			[5, 6, 0, -1],
			[0, 0, 14, 5]
		])
	})

	test('correctly evaluates table with circular referenced cells', () => {
		const table = [
			['d2', '6 b2 +', '2 3 ||', '5 a1 -'],
			['3 d1 *', 'e234', '5 1 2 + 4 * + 3 -', 'a2']
		]
		expect(evaluateTable(table)).toEqual([
			['#CYCLIC: d2 -> a2 -> d1', 6, '#ERR', '#CYCLIC: a1 -> d2 -> a2'],
			['#CYCLIC: d1 -> a1 -> d2', 0, 14, '#CYCLIC: a2 -> d1 -> a1']
		])
	})
})
