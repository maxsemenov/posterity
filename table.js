const operationRegexp = /[+\-*/]/
const cellRegexp = /[a-z]\d+/
const ERR_VALUE = '#ERR'

const columnNameToIndex = (columnKey) => columnKey.charCodeAt(0) - 97

class CyclicError extends Error {
	constructor(referenceStack, ...params) {
		super(...params)

		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, CyclicError)
		}

		this.name = 'CyclicError'
		// Custom debugging information
		this.referenceStack = [...referenceStack]
	}

	toString() {
		return `CyclicError: ${this.referenceStack.join(' -> ')}`
	}

	getReferenceStack() {
		return this.referenceStack
	}
}

const evaluateOperation = (arg1, arg2, operation) => {
	if (
		typeof arg1 !== 'number' ||
		typeof arg2 !== 'number' ||
		!operationRegexp.test(operation)
	) {
		throw new Error('invalid arguments')
	}

	switch (operation) {
		case '+':
			return arg1 + arg2

		case '-':
			return arg1 - arg2

		case '*':
			return arg1 * arg2

		case '/':
			return arg1 / arg2
	}
}

export const parsePostix = (postixStr) => {
	if (!postixStr) {
		throw new Error(`Incorrect postix expression: ${postixStr}`)
	}

	const operands = postixStr.split(' ').filter(Boolean)

	if (operands.length % 2 === 0) {
		throw new Error(`Incorrect postix expression: ${postixStr}`)
	}

	if (operands.length === 1 && !operationRegexp.test(operands[0])) {
		return [operands[0]]
	}

	const stack = []

	for (const operand of operands) {
		if (operationRegexp.test(operand)) {
			const arg2 = stack.pop()
			const arg1 = stack.pop()
			stack.push([arg1, arg2, operand])
		} else {
			stack.push(operand)
		}
	}

	if (stack.length !== 1) {
		throw new Error(`Cannot parse postix: ${postixStr}`)
	}

	return stack[0]
}

export const evaluatePostix = (postix, valueGetter = Number) => {
	let [arg1, arg2, operation] = postix

	arg1 = Array.isArray(arg1)
		? evaluatePostix(arg1, valueGetter)
		: valueGetter(arg1)
	if (!(arg2 && operation)) {
		return arg1
	}

	arg2 = Array.isArray(arg2)
		? evaluatePostix(arg2, valueGetter)
		: valueGetter(arg2)

	return evaluateOperation(arg1, arg2, operation)
}

export const evaluateTable = (table) => {
	let result = []
	let cache = []

	const getValue =
		(referenceTrack = []) =>
		(value) => {
			if (cellRegexp.test(value)) {
				if (referenceTrack.includes(value)) {
					throw new CyclicError(referenceTrack)
				}
				const [rowKey, ...columnStr] = value
				return getCellValue(Number(columnStr) - 1, columnNameToIndex(rowKey), [
					...referenceTrack,
					value
				])
			}
			const numValue = Number(value)

			if (Number.isNaN(numValue)) {
				throw new Error(`Incorrect numeric value: ${value}`)
			}
			return numValue
		}

	const getCellValue = (row, column, referenceTrack = []) => {
		let value = cache[row]?.[column]

		if (value == undefined) {
			const cellValue = table[row]?.[column]?.trim()
			value = cellValue
				? evaluatePostix(parsePostix(cellValue), getValue(referenceTrack))
				: 0

			if (!Array.isArray(cache[row])) {
				cache[row] = []
			}
			cache[row][column] = value
		}
		return value
	}

	for (let rowIndex = 0; rowIndex < table.length; rowIndex++) {
		const row = table[rowIndex]
		if (!Array.isArray(result[rowIndex])) {
			result[rowIndex] = []
		}

		if (!Array.isArray(row)) {
			throw new Error(`Could not process table row: ${row}`)
		}

		for (let cellIndex = 0; cellIndex < row.length; cellIndex++) {
			let value
			try {
				value = getCellValue(rowIndex, cellIndex)
			} catch (err) {
				if (err instanceof CyclicError) {
					value = `#CYCLIC: ${err.referenceStack.slice(0, -1).join(' -> ')}`
				} else {
					value = ERR_VALUE
				}
			}
			result[rowIndex][cellIndex] = value
		}
	}

	return result
}
